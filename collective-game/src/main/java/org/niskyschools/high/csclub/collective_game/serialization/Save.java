package org.niskyschools.high.csclub.collective_game.serialization;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * This is the marker interface used in order to save an object to disk.
 * In order to save an object,
 * @author rarkenin
 *
 */
public @interface Save {
	
}
